/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package emich.ilit.corpus.service.pojo;

import static org.uimafit.factory.AnalysisEngineFactory.createAggregateDescription;
import static org.uimafit.factory.AnalysisEngineFactory.createPrimitive;
import static org.uimafit.factory.AnalysisEngineFactory.createPrimitiveDescription;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

import edu.stanford.nlp.io.StringOutputStream;

import org.apache.uima.UIMAFramework;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.impl.XmiCasSerializer;
import org.apache.uima.examples.PrintAnnotations;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.examples.PrintAnnotations;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceSpecifier;
import org.apache.uima.util.FileUtils;
import org.apache.uima.util.InvalidXMLException;
import org.apache.uima.util.XMLInputSource;
import org.apache.uima.util.CasToInlineXml;
import org.apache.uima.util.XmlCasSerializer;
import org.xml.sax.SAXException;

import de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpPosTagger;
import de.tudarmstadt.ukp.dkpro.core.stanfordnlp.StanfordParser;
import de.tudarmstadt.ukp.dkpro.core.stanfordnlp.StanfordSegmenter;

//import emich.ilit.corpus.util.StringOutputStream;

/**
 * An example application that reads documents from files, sends them though an Analysis Engine, and
 * prints all discovered annotations to System.out and returns to SOAP
 * <p>
 * The processDescriptor takes two arguments:
 * <ol type="1">
 * <li>A document as String</li>
 * <li>The path to an XML descriptor for the Analysis Engine to be executed</li>
 * </ol>
 * The processDkProPipeline takes one argument
 * <ol type="1">
 * <li>A document as String</li>
 * </ol>
 */
public class PipelineProcessor {
	
	
  /**
   * Main program.
   * 
   * @param args
   *          Command-line arguments - see class description
 * @throws InvalidXMLException 
 * @throws ResourceInitializationException 
 * @throws AnalysisEngineProcessException 
 * @throws SAXException 
   */
	
  public static String processDescriptor(String document, String AE) throws IOException, InvalidXMLException, ResourceInitializationException, AnalysisEngineProcessException, SAXException{
	  
	  document = document.trim();
	  XMLInputSource in = new XMLInputSource(AE);
	  ResourceSpecifier specifier;
	  specifier = UIMAFramework.getXMLParser().parseResourceSpecifier(in);
	
	  AnalysisEngine ae = UIMAFramework.produceAnalysisEngine(specifier);
	  
   
   
     
	  //jcas from descriptor	
			JCas jcasAE = ae.newJCas();
			jcasAE.setDocumentLanguage("en");
			jcasAE.setDocumentText(document);
			ae.process(jcasAE);
			
		    OutputStream outputDesc = new StringOutputStream();
	        XmiCasSerializer.serialize(jcasAE.getCas(), outputDesc);
			
	  /*
	  // process with a plain CAS. Might only generate empty SOFA's
	  CAS cas = ae.newCAS();
	  ae.process(cas);
	  StringOutputStream sos = new StringOutputStream();
      XmlCasSerializer.serialize(cas, sos);
	  */
	   
   		
   	  //OutputStream output = new FileOutputStream("../someOutputXMI.out");
	  OutputStream output = new StringOutputStream();
      XmiCasSerializer.serialize(jcasAE.getCas(), output);

      System.out.println(output.toString());
      //PrintAnnotations.printAnnotations(cas, System.out);
     
      return output.toString();

	
  }
  
  public static String processDkProPipeline(String document) throws IOException, InvalidXMLException, ResourceInitializationException, AnalysisEngineProcessException, SAXException{

      //jcas from dkpro
   	  AnalysisEngineDescription stanSeg = createPrimitiveDescription(StanfordSegmenter.class);
   	  
   	  //tagger
   	  AnalysisEngineDescription tagger = createPrimitiveDescription(OpenNlpPosTagger.class);

   	  // setup parser for English
   	  /*
   	  AnalysisEngineDescription parser = createPrimitiveDescription(StanfordParser.class,
   			  		StanfordParser.PARAM_VARIANT, "factored",
   					StanfordParser.PARAM_PRINT_TAGSET, true,
   					StanfordParser.PARAM_CREATE_CONSTITUENT_TAGS, true,
   					StanfordParser.PARAM_CREATE_DEPENDENCY_TAGS, true,
   					StanfordParser.PARAM_CREATE_PENN_TREE_STRING, true,
   					StanfordParser.PARAM_CREATE_POS_TAGS, true);
		*/
   	AnalysisEngineDescription aggregate = createAggregateDescription(stanSeg, tagger);
   	  
   	  
        AnalysisEngine engine = createPrimitive(aggregate);
   		JCas jcas = engine.newJCas();
   		jcas.setDocumentLanguage("en");
   		jcas.setDocumentText(document);
   		engine.process(jcas);
   		
   	  //OutputStream output = new FileOutputStream("../someOutputXMI.out");
	     OutputStream output = new StringOutputStream();
         XmiCasSerializer.serialize(jcas.getCas(), output);
    

     System.out.println(output.toString());
     
     return output.toString();
     //return cas.getSofaDataString();
     //CasToInlineXml.generateXML(cas).trim();
     //return cas.toString();
	
  }
  
 public static String processAnalysisEngine(String document, String AE) throws 
 																IOException, 
 																InvalidXMLException, 
 																ResourceInitializationException, 
 																AnalysisEngineProcessException, 
 																SAXException
 	  {
	  
	  //trim the String
	  document = document.trim();
	  
	  //Read UIMA Analysis Engine Descriptor
	  XMLInputSource in = new XMLInputSource(AE);
	  ResourceSpecifier specifier = 
			  UIMAFramework.getXMLParser().parseResourceSpecifier(in);
	
	  // Produce the Analysis Engine
	  AnalysisEngine ae = UIMAFramework.produceAnalysisEngine(specifier);

	  // create a CAS Object
	  CAS cas = ae.newCAS();
	  
	  // process the CAS with the Analysis Engine
	  ae.process(cas);
	  
	  // serialize XMI of the CAS into a StringOutputStream
	  StringOutputStream sos = new StringOutputStream();
	  XmlCasSerializer.serialize(cas, sos);
     
	  // print it to tomcats log; both should work
	  // PrintAnnotations is from the example jar package
	  // System.out.println(sos.toString());
	  PrintAnnotations.printAnnotations(cas, System.out);

      // return the StringOutputStream as a String
	  return sos.toString();
	  
	  // some stuff you can try
	  //return cas.getSofaDataString();
	  //CasToInlineXml.generateXML(cas).trim();
	
  }
  
  
  public void processFolders(String AE, String dir) throws IOException{
  
        File taeDescriptor = new File(AE);
        File inputDir = new File(dir);

        // get Resource Specifier from XML file
        XMLInputSource in = new XMLInputSource(taeDescriptor);
        
        try{
        	ResourceSpecifier specifier = UIMAFramework.getXMLParser().parseResourceSpecifier(in);
	
        	// for debugging, output the Resource Specifier
        	// System.out.println(specifier);

        	// create Analysis Engine
        	AnalysisEngine ae = UIMAFramework.produceAnalysisEngine(specifier);
        	
        	// create a CAS
        	CAS cas = ae.newCAS();
        
        	// get all files in the input directory
        	File[] files = inputDir.listFiles();
        		if (files == null) {
        			System.out.println("No files to process");
        		} 
        		else 
        		{
        			// process documents
        			for (int i = 0; i < files.length; i++) {
        				if (!files[i].isDirectory()) {
        					processFile(files[i], ae, cas);
        					cas.reset();
            }
          }
        }
        ae.destroy();
	
        }
        catch (Exception e){
        	e.printStackTrace();
        }
  }

  /**
   * Prints usage message.
   */
  private static void printUsageMessage() {
    System.err.println("Usage: java org.apache.uima.example.ExampleApplication "
            + "<Analysis Engine descriptor or PEAR file name> <input dir>");
  }

  /**
   * Processes a single XML file and prints annotations to System.out
   * 
   * @param aFile
   *          file to process
   * @param aAE
   *          Analysis Engine that will process the file
   * @param aCAS
   *          CAS that will be used to hold analysis results
   */
  private static CAS processFile(File aFile, AnalysisEngine aAE, CAS aCAS) throws IOException,
          AnalysisEngineProcessException {
    System.out.println("Processing file " + aFile.getName());

    String document = FileUtils.file2String(aFile);
    document = document.trim();

    // put document text in CAS
    aCAS.setDocumentText(document);

    // process
    aAE.process(aCAS);

    // print annotations to System.out
    PrintAnnotations.printAnnotations(aCAS, System.out);

    return aCAS;

    // reset the CAS to prepare it for processing the next document
    // this is deprecated, happens now a step higher
    // aCAS.reset();
  }
}
